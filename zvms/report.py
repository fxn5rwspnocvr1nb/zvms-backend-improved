import datetime

from flask import Blueprint, request

from zvms.deco import *
from zvms.util import *

report = Blueprint('report', __name__)

@report.route('/report', methods=['POST'])
@Deco
def submitReport_NoToken(json_data, token_data):
    report = json_data.get('report')
    with open('./report.log', 'a+') as f:
        f.write(f'[{datetime.datetime.now()}]{json_data["report"]}\n')
    return success('提交成功')