from sqlalchemy import Integer, String, Text, SmallInteger, Column, DateTime, Date, create_engine
from sqlalchemy.ext.declarative import declarative_base

from zvms.util import ModelMixIn, render_markdown

with open('app.cfg') as f:
    old = create_engine(f.readline()[:-1])
    new = create_engine(f.readline()[:-1])

ModelOld = declarative_base()
ModelNew = declarative_base()
ModelOld.__engine__ = old
ModelNew.__engine__ = new

class ClassVol(ModelOld, ModelMixIn):
    __tablename__ = 'class_vol'
        
    volId = Column(Integer, primary_key=True)
    class_ = Column(Integer, primary_key=True, name='class')
    stuMax = Column(Integer)
    nowStuCount = Column(Integer)

    # def on_insert(self):
    #     ClassVolNew(
    #         cls_id=self.class_,
    #         vol_id=self.volId,
    #         max=self.stuMax
    #     ).insert()

    @property
    def volunteer(self):
        return Volunteer.query.get(self.volId).select('status', 'stuMax', markdown='description', volId='id', volName='name', volDate='date', volTime='time')

class StuVol(ModelOld, ModelMixIn):
    __tablename__ ='stu_vol'
        
    volId = Column(Integer, primary_key=True)
    stuId = Column(Integer, primary_key=True)
    status = Column(SmallInteger)
    volTimeInside = Column(Integer)
    volTimeOutside = Column(Integer)
    volTimeLarge = Column(Integer)
    thought = Column(Text)
    picture = Column(Text)

    # def on_insert(self):
    #     StuVolNew(
    #         stu_id=self.stuId,
    #         vol_id=self.volId,
    #         status=2,
    #         thought=''
    #     )

    # def on_update(self):
    #     reward = max(self.volTimeInside, self.volTimeOutside, self.volTimeLarge)
    #     StuVolNew.query.get((self.volId, self.stuId)).update(
    #         status=self.status,
    #         reward=reward,
    #         thought=self.thought
    #     )
    #     if self.picture is None:
    #         return
    #     for pic in self.picture.split(','):
    #         if not pic or PictureNew.query.get((self.volId, self.stuId, pic)):
    #             continue
    #         PictureNew(
    #             vol_id=self.volId,
    #             stu_id=self.stuId,
    #             hash=pic
    #         ).insert()

    @property
    def stuName(self):
        return Student.query.get(self.stuId).stuName

    @property
    def markdown(self):
        return render_markdown(self.thought)

class Student(ModelOld, ModelMixIn):
    __tablename__ = 'student'
        
    stuId = Column(Integer, primary_key=True)
    stuName = Column(String(64))
    activated_until = Column(Date)
    volTimeInside = Column(Integer)
    volTimeOutside = Column(Integer)
    volTimeLarge = Column(Integer)

    @property
    def volunteers(self):
        ret = list(StuVol.query.filter_by(stuId=self.stuId).select('volId', 'status', volTimeInside='inside', volTimeOutside='outside', volTimeLarge='large'))
        for i in ret:
            ret['name'] = Volunteer.query.get(ret['volId']).name
        return ret

class User(ModelOld, ModelMixIn):
    __tablename__ = 'user'

    # def on_update(self):
    #     UserNew.query.get(self.userId).update(
    #         pwd=self.password
    #     )
        
    userId = Column(Integer, primary_key=True, autoincrement=True)
    userName = Column(String(64))
    class_ = Column(Integer, name='class')
    permission = Column(Integer)
    notices = Column(Text)
    password = Column(String(155))

class UserNotice(ModelOld, ModelMixIn):
    __tablename__ = 'user_notice'
        
    noticeTitle = Column(Text)
    noticeText = Column(Text)
    deadTime = Column(Text)
    noticeId = Column(Integer, primary_key=True, autoincrement=True)

    @property
    def markdown(self):
        return render_markdown(self.noticeText)

class Volunteer(ModelOld, ModelMixIn):
    __tablename__ = 'volunteer'
        
    volId = Column(Integer, primary_key=True, autoincrement=True)
    volName = Column(String(255))
    volDate = Column(String(64))
    volTime = Column(String(64))
    stuMax = Column(Integer)
    nowStuCount = Column(Integer)
    description = Column(Text)
    status = Column(SmallInteger)
    volTimeInside = Column(Integer)
    volTimeOutside = Column(Integer)
    volTimeLarge = Column(Integer)
    holderId = Column(Integer)

    # def on_insert(self):
    #     reward = max(self.volTimeInside, self.volTimeOutside, self.volTimeLarge)
    #     type = 3
    #     if self.volTimeInside:
    #         type = 1
    #     elif self.volTimeOutside:
    #         type = 2
    #     VolunteerNew(
    #         id=self.volId,
    #         name=self.volName,
    #         description=self.description,
    #         holder_id=self.holderId,
    #         status=2,
    #         time=f'{self.volDate} {self.volTime}',
    #         type=type,
    #         reward=reward
    #     ).insert()

    @property
    def signers(self):
        return list(StuVol.query.filter_by(volId=self.volId).select('stuId', 'stuName'))

    @property
    def markdown(self):
        return render_markdown(self.description)

class ActivationCode(ModelOld, ModelMixIn):
    __tablename__ = 'activation_code'
    
    code = Column(Integer, primary_key=True)

class UserNew(ModelNew, ModelMixIn):
    __tablename__ = 'user'

    id = Column(Integer, primary_key=True)
    name = Column(String(5))
    cls_id = Column(Integer, name='class')
    pwd = Column(String(32))
    auth = Column(Integer)

class VolunteerNew(ModelNew, ModelMixIn):
    __tablename__ = 'volunteer'

    id = Column(Integer, primary_key=True)
    name = Column(String(32))
    description = Column(String(1024))
    holder_id = Column(Integer, name='holder')
    status = Column(SmallInteger)
    time = Column(DateTime)
    type = Column(SmallInteger)
    reward = Column(Integer)

class StuVolNew(ModelNew, ModelMixIn):
    __tablename__ = 'stu_vol'

    stu_id = Column(Integer, primary_key=True)
    vol_id = Column(Integer, primary_key=True)
    status = Column(SmallInteger)
    thought = Column(String(1024))
    reason = Column(String(1024))
    reward = Column(Integer)

class ClassVolNew(ModelNew, ModelMixIn):
    __tablename__ = 'class_vol'

    cls_id = Column(Integer, primary_key=True, name='class_id')
    vol_id = Column(Integer, primary_key=True)
    max = Column(Integer)

class PictureNew(ModelNew, ModelMixIn):
    __tablename__ = 'picture'

    vol_id = Column(Integer, primary_key=True)
    stu_id = Column(Integer, primary_key=True)
    hash = Column(String(32), primary_key=True)

ModelNew.metadata.create_all(new)
ModelOld.metadata.create_all(old)