from flask import Blueprint
from sqlalchemy import or_, and_

from zvms.models import User, Student, StuVol, ClassVol
from zvms.deco import Deco
from zvms.res import *
from zvms.util import *

class_ = Blueprint('class', __name__)

@class_.route('/class/list', methods=['GET'])
@Deco
def getClassList(json_data, token_data):
    r = sorted(list(User.query.filter(User.class_ > 200000).select(class_='id')), key=lambda x: x['id'])
    if not r:
        return not_found
    for i in r:
        i['name'] = classIdToString(i['id'])
    return success('获取成功', **{'class': r})

@class_.route('/class/stulist/<int:classId>', methods=['GET'])
@Deco
def getStudentList(classId, json_data, token_data):
    r = list(Student.query.filter(Student.stuId.between(classId * 100, classId * 100 + 100)).select(
        stuId='id', stuName='name', volTimeInside='inside', volTimeOutside='outside', volTimeLarge='large'))
    if not r:
        return not_found
    return success('获取成功', student=r)

@class_.route('/class/volunteer/<int:classId>', methods=['GET'])
@Deco
def getStudentVolunteer(classId, json_data, token_data):
    r = list(ClassVol.query.filter_by(class_=classId).select_value('volunteer'))
    if not r:
        return not_found
    return success('获取成功', volunteer=r)
    
@class_.route('/class/noThought/<int:classId>', methods=['GET'])
@Deco
def getNoThought(classId, json_data, token_data):
    r = list(filter(lambda sv: sv['stuId'] // 100 == classId, 
        StuVol.query.filter(or_(StuVol.status == STATUS_RESUBMIT, and_(StuVol.status == STATUS_WAITING,
        StuVol.thought == ''))).select('volId', 'stuId')))
    if not r:
        return success('没有需要填写的义工')
    return success('获取成功', result=r)