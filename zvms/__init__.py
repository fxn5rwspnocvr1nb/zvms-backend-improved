from flask import Flask
from flask_cors import CORS

from zvms.res import STATIC_FOLDER

app = Flask(__name__)
with open('app.cfg') as f:
    app.config['SECRET_KEY'] = f.readlines()[2][:-1]
CORS(app, supports_credentials=True, resources={r"/*", "*"})
app.static_folder = STATIC_FOLDER

app.test_request_context().push()

from zvms.user import user
from zvms.class_ import class_
from zvms.student import student
from zvms.volunteer import volunteer
from zvms.report import report

app.register_blueprint(user)
app.register_blueprint(class_)
app.register_blueprint(student)
app.register_blueprint(volunteer)
app.register_blueprint(report)