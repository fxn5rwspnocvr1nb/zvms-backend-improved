from random import randrange
import pymysql

codes = []
with open('codes.txt', 'w') as f:
    for i in range(500):
        code = randrange(0, 0xffff)
        f.write(f'{code} ')
        codes.append(code)


conn = pymysql.connect(
    host='localhost',
    user='qnc',
    database='zvms',
    password='123456'
)
cur = conn.cursor()

for code in codes:
    cur.execute('INSERT INTO activation_code VALUES(%s)', (code, ))
conn.commit()